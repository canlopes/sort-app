import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SortItems {
	public static WebDriver browser;

	public static List<WebElement> getListElements(String str) {
		List<WebElement> listElement = browser.findElements(By.tagName(str));
		for (int i = 0; i < listElement.size(); i++) {
			String elementText = listElement.get(i).getText();
			System.out.println("getListElements[" + i + "][" + elementText + "]");// debug
		}
		return listElement;

	}

	public static void sort() {

		DesiredCapabilities dc = DesiredCapabilities.firefox();
		dc.setBrowserName(Constants.FIREFOX);
		try {
			browser = new RemoteWebDriver(new URL(Constants.FIREFOX_HOST), dc);
			System.out.println("Firefox session created!");// debug
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		browser.get(Constants.SERVER_URL);

		Pattern reg_pat = Pattern.compile(Constants.PATTERN);
		boolean all_sorted = false;
		do {
			int current_pos = 0;
			int new_pos = 0;
			List<WebElement> listElements = getListElements(Constants.HTML_SEARCH_TAG);
			all_sorted = true;
			for (WebElement element : listElements) {
				current_pos = listElements.indexOf(element);
				String current_text = element.getText();
				Matcher req_match = reg_pat.matcher(element.getText());
				if (req_match.find()) {
					new_pos = Integer.parseInt(req_match.group(0));
				}

				if (new_pos != current_pos) {
					Actions builder = new Actions(browser);
					Action dragAndDrop = builder.clickAndHold(listElements.get(current_pos))
							.moveToElement(listElements.get(new_pos)).release(listElements.get(new_pos)).build();
					dragAndDrop.perform();
					System.out.println("Moving: [" + current_text + "] from " + current_pos + " to " + new_pos);// debug
					all_sorted = false;
					break;
				} else {
					System.out.println("Nothing to do: [" + current_text + "] from " + current_pos + " to " + new_pos);// debug
				}
			}

		} while (!(all_sorted));
		System.out.println("All sorted. The End!");// debug

	}

	
	public static void main(String[] args) {
		sort();
	}
}
