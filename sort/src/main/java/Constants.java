
public class Constants {
	public static final String FIREFOX = "firefox";
	public static final String FIREFOX_HOST = "http://selenium:4444/wd/hub";
	public static final String SERVER_URL = "http://web:3000";
	public static final String PATTERN = "-?\\d+";
	public static final String HTML_SEARCH_TAG = "li";
}
