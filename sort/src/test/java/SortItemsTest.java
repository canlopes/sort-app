
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import junit.framework.TestCase;

public class SortItemsTest extends TestCase {
	@Override
	protected void setUp() {
		SortItems.sort();

	}

	public void testSort() {

		List<WebElement> listElements = SortItems.getListElements(Constants.HTML_SEARCH_TAG);

		List<String> strings = new ArrayList<String>();
		for (WebElement e : listElements) {
			strings.add(e.getText());
		}
		assertTrue(isSorted(strings));
		SortItems.browser.close();
	}

	boolean isSorted(List<String> lst) {
		String previous = "";

		for (final String current : lst) {
			if (current.compareTo(previous) < 0)
				return false;
			previous = current;
		}

		return true;
	}

}
