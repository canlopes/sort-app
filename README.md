# sort-app

A simple sorter

## Getting Started

[Download](https://gitlab.com/canlopes/sort-app/-/archive/master/sort-app-master.zip) the code run it with
```
docker-compose up --build
```
### Prerequisites

```
docker
docker-compose
```


## How it works

 - The docker-compose command will launch:
   - the node app provided for this challenge at port 3000
   - a selenium standalone-firefox
   - the sort-app

The sort-app, will get all the **\<li\>** elements on the webpage provided by the node app, and get them sorted.
 